/*
 * Copyright (C) 2017
 *      Andrew Hayzen <ahayzen@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.4
import QtQuick.Window 2.0


Window {
    height: 400
    minimumHeight: 410  // FIXME: use grid units by removing Main abstraction ?
    minimumWidth: 400  // TODO: make gameview countdown better with width
    title: "Kana Game"
    width: 400

    Loader {
        anchors {
            fill: parent
        }
        source: Qt.resolvedUrl("QtMain.qml")
        // TODO: de-ubuntu the code
        //source: {
        //    if (Qt.platform.os === "Ubuntu") {
        //        Qt.resolvedUrl("UbuntuMain.qml")
        //    } else {
        //        Qt.resolvedUrl("QtMain.qml")
        //    }
        //}

        Component.onCompleted: {
            console.debug("PLATFORM:", Qt.platform.os)
        }
    }
}
