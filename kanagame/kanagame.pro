TEMPLATE = aux
TARGET = kanagame

RESOURCES += kanagame.qrc

QML_FILES += $$files(*.qml,true) \
             $$files(*.js,true)

CONF_FILES +=  kanagame.apparmor \
               kanagame.png

AP_TEST_FILES += tests/autopilot/run \
                 $$files(tests/*.py,true)

OTHER_FILES += $${CONF_FILES} \
               $${QML_FILES} \
               $${AP_TEST_FILES} \
               kanagame.desktop

#specify where the qml/js files are installed to
root_qml_files.path = /kanagame
root_qml_files.files += $$files(*.qml,false) \
                        $$files(*.js,false)

comp_qml_files.path = /kanagame/components
comp_qml_files.files += $$files(components/*.qml,false) \
                        $$files(components/*.js,false)

comp_actions_qml_files.path = /kanagame/components/actions
comp_actions_qml_files.files += $$files(components/actions/*.qml,false) \
                        $$files(components/actions/*.js,false)

logic_qml_files.path = /kanagame/logic
logic_qml_files.files += $$files(logic/*.qml,false) \
                        $$files(logic/*.js,false)

theme_qml_files.path = /kanagame/theme
theme_qml_files.files += $$files(theme/*.qml,false) \
                         $$files(theme/*.js,false)

ui_qml_files.path = /kanagame/ui
ui_qml_files.files += $$files(ui/*.qml,false) \
                        $$files(ui/*.js,false)

ui_game_qml_files.path = /kanagame/ui/game
ui_game_qml_files.files += $$files(ui/game/*.qml,false) \
                        $$files(ui/game/*.js,false)

#specify where the config files are installed to
config_files.path = /kanagame
config_files.files += $${CONF_FILES}

#install the desktop file, a translated version is 
#automatically created in the build directory
desktop_file.path = /kanagame
desktop_file.files = $$OUT_PWD/kanagame.desktop
desktop_file.CONFIG += no_check_exist

INSTALLS+=config_files root_qml_files comp_qml_files comp_actions_qml_files logic_qml_files theme_qml_files ui_qml_files ui_game_qml_files desktop_file

DISTFILES += \
    components/actions/KanaAction.qml \
    components/actions/KanaHeadAction.qml \
    components/KanaHeader.qml \
    components/KanaActionBar.qml \
    components/KanaButton.qml \
    components/KanaGrid.qml \
    components/KanaMenu.qml \
    components/KanaPage.qml \
    components/KanaSelectableRow.qml \
    components/KanaSpinBox.qml \
    components/KanaStack.qml \
    logic/enum.js \
    logic/kana.js \
    logic/kanamap.js \
    theme/KanaTheme.qml \
    theme/LightPalette.qml \
    theme/KanaPalette.qml \
    theme/DarkPalette.qml \
    ui/game/Game.qml \
    ui/game/GameFinish.qml \
    ui/game/GameSelection.qml \
    ui/Help.qml \
    ui/HelpChart.qml \
    ui/MainMenu.qml \
    ui/game/GameTimerBar.qml \
    GenericMain.qml \
    Main.qml \
    QtMain.qml \
    UbuntuMain.qml

