/*
 * Copyright (C) 2017
 *      Andrew Hayzen <ahayzen@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.4

Row {
    height: settings.display.gu(3)
    width: settings.display.gu(8)

    property real minimum
    property real maximum
    property real step
    property real value

    readonly property real internalWidth: width / 3

    signal newValue(real newValue)

    Rectangle {
        color: kanaTheme.palette.widget
        height: parent.height
        opacity: decreaseMouseArea.enabled ? 1 : 0.4
        width: internalWidth

        Text {
            anchors {
                fill: parent
            }
            font {
                pixelSize: settings.display.gu(2)
            }
            horizontalAlignment: Text.AlignHCenter
            text: "-"
            verticalAlignment: Text.AlignVCenter
        }

        MouseArea {
            id: decreaseMouseArea
            anchors {
                fill: parent
            }
            enabled: value > minimum
            onClicked: {
                if (value - step >= minimum) {
                    newValue(value - step);
                } else {
                    newValue(minimum);
                }
            }
        }
    }


    Text {
        color: kanaTheme.palette.backgroundText
        font {
            pixelSize: settings.display.gu(2)
        }
        height: parent.height
        horizontalAlignment: Text.AlignHCenter
        text: "あ" //value.toFixed(0)
        verticalAlignment: Text.AlignVCenter
        width: internalWidth
    }

    Rectangle {
        color: kanaTheme.palette.widget
        height: parent.height
        opacity: increaseMouseArea.enabled ? 1 : 0.4
        width: internalWidth

        Text {
            anchors {
                fill: parent
            }
            font {
                pixelSize: settings.display.gu(2)
            }
            horizontalAlignment: Text.AlignHCenter
            text: "+"
            verticalAlignment: Text.AlignVCenter
        }

        MouseArea {
            id: increaseMouseArea
            anchors {
                fill: parent
            }
            enabled: value < maximum
            onClicked: {
                if (value + step <= maximum) {
                    newValue(value + step);
                } else {
                    newValue(maximum);
                }
            }
        }
    }
}
