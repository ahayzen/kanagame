/*
 * Copyright (C) 2017
 *      Andrew Hayzen <ahayzen@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.4

Item {
    default property alias children: childrenContainer.children
    readonly property int depth: priv.stack.length

    QtObject {
        id: priv

        property var stack: []
    }

    function hideObject(obj) {
        if (obj) {
            obj.opacity = 0;
        }
    }

    function pop(amount) {
        if (amount === undefined) {
            amount = 1;
        }

        for (var i=0; i < amount; i++) {
            if (priv.stack.length > 1) {
                hideObject(top());

                container.children = priv.stack[priv.stack.length - 2];
                priv.stack.pop().destroy();

                showObject(top());
            } else {
                console.warn("Attempted to pop from stack when there is only one item");
            }
        }
    }

    function popToRoot() {
        pop(Math.max(priv.stack.length - 1, 0))
    }

    function push(url, properties) {
        var component = Qt.createComponent(url);

        if (component.status == Component.Error) {
            console.log("Error loading component:", component.errorString());
        }

        var object = component.createObject(container, properties || {});

        if (!object) {
            console.log("Sprite is null");
        }

        hideObject(top());

        container.children = object;
        priv.stack.push(object);
    }

    function showObject(obj) {
        if (obj) {
            obj.opacity = 1;
        }
    }

    function top() {
        return priv.stack.length > 0 ? priv.stack[priv.stack.length - 1] : null
    }

    Item {
        id: childrenContainer
        anchors {
            fill: parent
        }
    }

    Item {
        id: container
        anchors {
            fill: parent
        }
    }
}

