/*
 * Copyright (C) 2017
 *      Andrew Hayzen <ahayzen@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.4
import "actions"

Item {
    anchors {
        left: parent.left
        right: parent.right
        top: parent.top
    }
    height: settings.display.gu(5) + (sections.length ? sectionsRow.height : 0)

    property list<KanaHeadAction> leadingActions
    property list<KanaHeadAction> sections
    property int sectionIndex: 0
    property alias title: titleText.text
    property list<KanaHeadAction> trailingActions

    KanaActionBar {
        actions: leadingActions
        anchors {
            left: parent.left
            top: parent.top
        }
        height: settings.display.gu(5)
    }

    Text {
        id: titleText
        anchors {
            horizontalCenter: parent.horizontalCenter
        }
        color: kanaTheme.palette.headerText
        font {
            pixelSize: settings.display.gu(4)
        }
        height: settings.display.gu(5)
        verticalAlignment: Text.AlignVCenter
    }

    KanaActionBar {
        actions: trailingActions
        anchors {
            right: parent.right
            top: parent.top
        }
        height: settings.display.gu(5)
    }

    Row {
        id: sectionsRow
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }
        height: settings.display.gu(4)
        spacing: settings.display.gu(1)
        visible: sections.length

        Repeater {
            delegate: Rectangle {
                color: index === sectionIndex ? modelData.hintColor : "transparent"
                height: parent.height
                width: sectionsRow.width / sections.length

                Text {
                    anchors {
                        centerIn: parent
                    }
                    color: kanaTheme.palette.headerText
                    elide: Text.ElideMiddle
                    font {
                        pixelSize: settings.display.gu(1.5)
                    }
                    horizontalAlignment: Text.AlignHCenter
                    text: modelData.text
                    width: parent.width
                }

                Rectangle {
                    anchors {
                        bottom: parent.bottom
                        left: parent.left
                        right: parent.right
                    }
                    color: modelData.hintColor === "transparent" ? theme.palette.widget : modelData.hintColor
                    height: settings.display.gu(0.125)
                }

                MouseArea {
                    anchors {
                        fill: parent
                    }
                    onClicked: sectionIndex = index
                }
            }
            model: sections
        }
    }
}
