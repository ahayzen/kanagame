/*
 * Copyright (C) 2017
 *      Andrew Hayzen <ahayzen@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.4

import "actions"

KanaGrid {
    anchors {
        bottom: parent.bottom
        horizontalCenter: parent.horizontalCenter
        top: parent.bottom
        topMargin: settings.display.gu(4)
    }
    model: actions

    // number of columns that fits in parent width (min=1) * cellWidth
    width: Math.max(Math.min(Math.floor(parent.width / cellWidth), count), 1) * cellWidth

    property list<KanaAction> actions
}
