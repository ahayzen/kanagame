/*
 * Copyright (C) 2017
 *      Andrew Hayzen <ahayzen@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.4

import "actions"

Item {
    height: settings.display.gu(16)
    width: height

    property KanaAction action

    Rectangle {
        id: container
        anchors {
            fill: parent
            margins: settings.display.gu(1)
        }
        border {
            color: action.hintColor

            Behavior on color {
                ColorAnimation {

                }
            }
        }
        color: "transparent"
        height: settings.display.gu(14)
        width: height

        Rectangle {
            anchors {
                fill: parent
            }
            color: action.hintColor
            opacity: 0.2

            Behavior on color {
                ColorAnimation {

                }
            }
        }

        Column {
            anchors {
                centerIn: parent
            }
            width: parent.width

            Text {
                font {
                    pixelSize: settings.display.gu(4)
                }
                color: action.textColor === "transparent" ? "#FFF" : action.textColor
                elide: Text.ElideRight
                horizontalAlignment: Text.AlignHCenter
                text: action.text
                width: parent.width
            }

            Text {
                font {
                    pixelSize: settings.display.gu(2)
                }
                color: action.textColor === "transparent" ? "#FFF" : action.textColor
                elide: Text.ElideRight
                horizontalAlignment: Text.AlignHCenter
                text: action.subtext
                width: parent.width
                visible: text !== ""
            }
        }

        Text {
            anchors {
                bottom: parent.bottom
                left: parent.left
                margins: settings.display.gu(1)
                right: parent.right
            }
            color: action.textColor === "transparent" ? "#FFF" : action.textColor
            font {
                pixelSize: settings.display.gu(1.5)
            }
            elide: Text.ElideRight
            horizontalAlignment: Text.AlignRight
            opacity: 0.5
            text: action.hintText
            visible: text !== ""
        }

        MouseArea {
            id: mouseArea
            anchors {
                fill: parent
            }
            onClicked: action.triggered()
        }
    }
}
