/*
 * Copyright (C) 2017
 *      Andrew Hayzen <ahayzen@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.4

MouseArea {
    id: mouseArea

    property alias text: selectableText.text

    Row {
        anchors {
            fill: parent
        }
        spacing: settings.display.gu(1)

        Text {
            id: selectorIndicator
            color: mouseArea.selected ? kanaTheme.palette.positive : kanaTheme.palette.negative
            font {
                pixelSize: parent.height - settings.display.gu(1)
            }
            height: parent.height - settings.display.gu(1)
            horizontalAlignment: Text.AlignLeft
            text: mouseArea.selected ? "✔" : "–"
            verticalAlignment: Text.AlignVCenter
            width: height
        }

        Text {
            id: selectableText
            color: kanaTheme.palette.backgroundText
            elide: Text.ElideRight
            font {
                pixelSize: parent.height - settings.display.gu(1.5)
            }
            height: parent.height
            horizontalAlignment: Text.AlignLeft
            opacity: mouseArea.selected ? 1 : 0.5
            verticalAlignment: Text.AlignVCenter
            width: parent.width - selectorIndicator.width - parent.spacing

            Behavior on opacity {
                NumberAnimation {

                }
            }
        }
    }
}
