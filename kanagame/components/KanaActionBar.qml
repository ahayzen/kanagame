/*
 * Copyright (C) 2017
 *      Andrew Hayzen <ahayzen@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.4

Row {
    property alias actions: repeater.model

    Repeater {
        id: repeater
        delegate: Item {
            anchors {
                bottom: parent.bottom
                top: parent.top
            }
            opacity: modelData.enabled ? 1 : 0.4
            width: row.anchors.leftMargin + row.width + itemSpacing

            property int itemSpacing: settings.display.gu(2)

            Rectangle {
                anchors {
                    fill: parent
                }
                color: modelData.hintColor === "transparent" ? theme.palette.widget : modelData.hintColor
                opacity: mouseArea.pressed ? 0.5 : 0.2
            }

            Row {
                id: row
                anchors {
                    bottom: parent.bottom
                    left: parent.left
                    leftMargin: itemSpacing
                    top: parent.top
                }
                spacing: itemSpacing

                Text {
                    anchors {
                        verticalCenter: parent.verticalCenter
                    }
                    font {
                        pixelSize: settings.display.gu(3)
                    }
                    color: modelData.textColor === "transparent" ? theme.palette.widgetText : modelData.textColor
                    text: modelData.text
                }
            }

            Rectangle {
                anchors {
                    bottom: parent.bottom
                    left: parent.left
                    right: parent.right
                }
                color: modelData.hintColor === "transparent" ? theme.palette.widget : modelData.hintColor
                height: settings.display.gu(0.125)
            }

            MouseArea {
                id: mouseArea
                anchors {
                    fill: parent
                }
                enabled: modelData.enabled

                onClicked: modelData.triggered()
            }
        }
    }
}
