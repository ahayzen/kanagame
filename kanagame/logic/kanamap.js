/*
 * Copyright (C) 2017
 *      Andrew Hayzen <ahayzen@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

.pragma library

// Hiragana

var hira_vowels = [
    ["a", "あ"],
    ["i", "い"],
    ["u", "う"],
    ["e", "え"],
    ["o", "お"],
]
var hira_ka     = [
    ["ka", "か"],
    ["ki", "き"],
    ["ku", "く"],
    ["ke", "け"],
    ["ko", "こ"],
]
var hira_sa     = [
    ["sa", "さ"],
    ["shi", "し"],
    ["su", "す"],
    ["se", "せ"],
    ["so", "そ"],
]
var hira_ta     = [
    ["ta", "た"],
    ["chi", "ち"],
    ["tsu", "つ"],
    ["te", "て"],
    ["to", "と"],
]
var hira_na     = [
    ["na", "な"],
    ["ni", "に"],
    ["nu", "ぬ"],
    ["ne", "ね"],
    ["no", "の"],
]
var hira_ha     = [
    ["ha", "は"],
    ["hi", "ひ"],
    ["fu", "ふ"],
    ["he", "へ"],
    ["ho", "ほ"],
]
var hira_ma     = [
    ["ma", "ま"],
    ["mi", "み"],
    ["mu", "む"],
    ["me", "め"],
    ["mo", "も"],
]
var hira_ya     = [
    ["ya", "や"],
    ["yu", "ゆ"],
    ["yo", "よ"],
]
var hira_ra     = [
    ["ra", "ら"],
    ["ri", "り"],
    ["ru", "る"],
    ["re", "れ"],
    ["ro", "ろ"],
]
var hira_wa     = [
    ["wa", "わ"],
    ["wo", "を"],
    ["n", "ん"],
]
var hira_ga     = [
    ["ga", "が"],
    ["gi", "ぎ"],
    ["gu", "ぐ"],
    ["ge", "げ"],
    ["go", "ご"],
]
var hira_za     = [
    ["za", "ざ"],
    ["ji", "じ"],
    ["zu", "ず"],
    ["ze", "ぜ"],
    ["zo", "ぞ"],
]
var hira_da     = [
    ["da", "だ"],
    ["di", "ぢ"],
    ["du", "づ"],
    ["de", "で"],
    ["do", "ど"],
]
var hira_ba     = [
    ["ba", "ば"],
    ["bi", "び"],
    ["bu", "ぶ"],
    ["be", "べ"],
    ["bo", "ぼ"],
]
var hira_pa     = [
    ["pa", "ぱ"],
    ["pi", "ぴ"],
    ["pu", "ぷ"],
    ["pe", "ぺ"],
    ["po", "ぽ"],
]
var hira_kya    = [
    ["kya", "きゃ"],
    ["kyu", "きゅ"],
    ["kyo", "きょ"],
]
var hira_sha    = [
    ["sha", "しゃ"],
    ["shu", "しゅ"],
    ["sho", "しょ"],
]
var hira_cha    = [
    ["cha", "ちゃ"],
    ["chu", "ちゅ"],
    ["cho", "ちょ"],
]
var hira_nya    = [
    ["nya", "にゃ"],
    ["nyu", "にゅ"],
    ["nyo", "にょ"],
]
var hira_hya    = [
    ["hya", "ひゃ"],
    ["hyu", "ひゅ"],
    ["hyo", "ひょ"],
]
var hira_mya    = [
    ["mya", "みゃ"],
    ["myu", "みゅ"],
    ["myo", "みょ"],
]
var hira_rya    = [
    ["rya", "りゃ"],
    ["ryu", "りゅ"],
    ["ryo", "りょ"],
]
var hira_gya    = [
    ["gya", "ぎゃ"],
    ["gyu", "ぎゅ"],
    ["gyo", "ぎょ"],
]
var hira_ja    = [
    ["ja", "じゃ"],
    ["ju", "じゅ"],
    ["jo", "じょ"],
]
var hira_bya    = [
    ["bya", "びゃ"],
    ["byu", "びゅ"],
    ["byo", "びょ"],
]
var hira_pya    = [
    ["pya", "ぴゃ"],
    ["pyu", "ぴゅ"],
    ["pyo", "ぴょ"],
]

var hira = [
    hira_vowels,
    hira_ka,
    hira_sa,
    hira_ta,
    hira_na,
    hira_ha,
    hira_ma,
    hira_ya,
    hira_ra,
    hira_wa,
    hira_ga,
    hira_za,
    hira_da,
    hira_ba,
    hira_pa,
    hira_kya,
    hira_sha,
    hira_cha,
    hira_nya,
    hira_hya,
    hira_mya,
    hira_rya,
    hira_gya,
    hira_ja,
    hira_bya,
    hira_pya,
]


// Katakana

var kata_vowels = [
    ["a", "ア"],
    ["i", "イ"],
    ["u", "ウ"],
    ["e", "エ"],
    ["o", "オ"],
]
var kata_ka     = [
    ["ka", "カ"],
    ["ki", "キ"],
    ["ku", "ク"],
    ["ke", "ケ"],
    ["ko", "コ"],
]
var kata_sa     = [
    ["sa", "サ"],
    ["shi", "シ"],
    ["su", "ス"],
    ["se", "セ"],
    ["so", "ソ"],
]
var kata_ta     = [
    ["ta", "タ"],
    ["chi", "チ"],
    ["tsu", "ツ"],
    ["te", "テ"],
    ["to", "ト"],
]
var kata_na     = [
    ["na", "ナ"],
    ["ni", "ニ"],
    ["nu", "ヌ"],
    ["ne", "ネ"],
    ["no", "ノ"],
]
var kata_ha     = [
    ["ha", "ハ"],
    ["hi", "ヒ"],
    ["fu", "フ"],
    ["he", "ヘ"],
    ["ho", "ホ"],
]
var kata_ma     = [
    ["ma", "マ"],
    ["mi", "ミ"],
    ["mu", "ム"],
    ["me", "メ"],
    ["mo", "モ"],
]
var kata_ya     = [
    ["ya", "ヤ"],
    ["yu", "ユ"],
    ["yo", "ヨ"],
]
var kata_ra     = [
    ["ra", "ラ"],
    ["ri", "リ"],
    ["ru", "ル"],
    ["re", "レ"],
    ["ro", "ロ"],
]
var kata_wa     = [
    ["wa", "ワ"],
    ["wo", "ヲ"],
    ["n", "ン"]
]
var kata_ga     = [
    ["ga", "ガ"],
    ["gi", "ギ"],
    ["gu", "グ"],
    ["ge", "ゲ"],
    ["go", "ゴ"],
]
var kata_za     = [
    ["za", "ザ"],
    ["ji", "ジ"],
    ["zu", "ズ"],
    ["ze", "ゼ"],
    ["zo", "ゾ"],
]
var kata_da     = [
    ["da", "ダ"],
    ["di", "ヂ"],
    ["du", "ヅ"],
    ["de", "デ"],
    ["do", "ド"],
]
var kata_ba     = [
    ["ba", "バ"],
    ["bi", "ビ"],
    ["bu", "ブ"],
    ["be", "ベ"],
    ["bo", "ボ"],
]
var kata_pa     = [
    ["pa", "パ"],
    ["pi", "ピ"],
    ["pu", "プ"],
    ["pe", "ペ"],
    ["po", "ポ"],
]
var kata_kya     = [
    ["kya", "キャ"],
    ["kyu", "キュ"],
    ["kyo", "キョ"],
]
var kata_sha     = [
    ["sha", "シャ"],
    ["shu", "シュ"],
    ["sho", "ショ"],
]
var kata_cha     = [
    ["cha", "チャ"],
    ["chu", "チュ"],
    ["cho", "チョ"],
]
var kata_nya     = [
    ["nya", "ニャ"],
    ["nyu", "ニュ"],
    ["nyo", "ニョ"],
]
var kata_hya     = [
    ["hya", "ヒャ"],
    ["hyu", "ヒュ"],
    ["hyo", "ヒョ"],
]
var kata_mya     = [
    ["mya", "ミャ"],
    ["myu", "ミュ"],
    ["myo", "ミョ"],
]
var kata_rya     = [
    ["rya", "リャ"],
    ["ryu", "リュ"],
    ["ryo", "リョ"],
]
var kata_gya     = [
    ["gya", "ギャ"],
    ["gyu", "ギュ"],
    ["gyo", "ギョ"],
]
var kata_ja     = [
    ["ja", "ジャ"],
    ["ju", "ジュ"],
    ["jo", "ジョ"],
]
var kata_bya     = [
    ["bya", "ビャ"],
    ["byu", "ビュ"],
    ["byo", "ビョ"],
]
var kata_pya     = [
    ["pya", "ピャ"],
    ["pyu", "ピュ"],
    ["pyo", "ピョ"],
]

var kata = [
    kata_vowels,
    kata_ka,
    kata_sa,
    kata_ta,
    kata_na,
    kata_ha,
    kata_ma,
    kata_ya,
    kata_ra,
    kata_wa,
    kata_ga,
    kata_za,
    kata_da,
    kata_ba,
    kata_pa,
    kata_kya,
    kata_sha,
    kata_cha,
    kata_nya,
    kata_hya,
    kata_mya,
    kata_rya,
    kata_gya,
    kata_ja,
    kata_bya,
    kata_pya,
]
