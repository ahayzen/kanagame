/*
 * Copyright (C) 2017
 *      Andrew Hayzen <ahayzen@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
.pragma library

function concat(listOfLists)
{
    return [].concat.apply([], listOfLists);
}

function extractKana(list)
{
    var out = [];

    for (var i=0; i < list.length; i++) {
        out.push(extractKanaSingle(list[i]));
    }

    return out;
}

function extractKanaSingle(item)
{
    return item[1];
}

function extractRomaji(list)
{
    var out = [];

    for (var i=0; i < list.length; i++) {
        out.push(extractRomajiSingle(list[i]));
    }

    return out;
}

function extractRomajiSingle(item)
{
    return item[0];
}

function random(max)
{
    return Math.floor(Math.random() * (max + 1));
}

function randomise(l)
{
    var out = [];
    var list = l.slice();

    while (list.length > 0) {
        var i = random(list.length - 1);

        out.push(list[i]);
        list.splice(i, 1);
    }

    return out;
}

function round(list, answer)
{
    var options = [answer];

    while (options.length < 4) {
        var option = list[random(list.length - 1)];

        // Check the option is not the answer and ensure no duplicates
        if (option[0] !== answer[0] && option[1] !== answer[1] && options.indexOf(option) === -1) {
            options.push(option);
        } else if (options.length > list.length - 1) {
            // All options have been used, fill with null's
            options.push("-")
        }
    }

    return randomise(options);
}
