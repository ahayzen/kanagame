/*
 * Copyright (C) 2017
 *      Andrew Hayzen <ahayzen@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.0

QtObject {
    property string background: "#FFF"
    property string backgroundText: "#000"

    property string header: background
    property string headerText: backgroundText

    property string negative: "#F00"
    property string negativeText: "#000"

    property string neutral: "#3CF"
    property string neutralText: "#000"

    property string positive: "#090"
    property string positiveText: "#000"

    property string selected: "#F60"
    property string selectedText: "#000"

    property string widget: "#CCC"
    property string widgetText: "#000"
}
