/*
 * Copyright (C) 2017
 *      Andrew Hayzen <ahayzen@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.4
import Qt.labs.settings 1.0

import "components"
import "theme"
import "ui"

KanaStack {
    id: kanaStack
    anchors {
        fill: parent
    }

    onHeightChanged: settings.display.checkScalar()
    onWidthChanged: settings.display.checkScalar()

    property QtObject settings: QtObject {
        property Settings display: Settings {
            category: "display"

            property bool fullscreen: false
            property real scalar: 8.0

            function gu(unit) {
                return unit * scalar;
            }

            function checkScalar() {
                if ((kanaStack.width <= 500 || kanaStack.height <= 700) && settings.display.scalar === 16) {
                    settings.display.scalar = 10;
                }
            }
        }

        property Settings game: Settings {
            category: "game"

            // Store as comma separated string and no array as settings don't
            // actually store the array
            // Start with just selecting the first row of hiragana
            property string selection: "0-0"
        }

        property Settings highScore: Settings {
            category: "scores"

            property int kanaToRomaji: 0
            property int romajiToKana: 0
        }

        property Settings theme: Settings {
            category: "theme"

            property string name: "Light"
        }

        property int version: 1
    }

    KanaTheme {
        id: kanaTheme
        name: settings.theme.name
    }

    Rectangle {
        anchors {
            fill: parent
        }
        color: kanaTheme.palette.background
    }

    Component.onCompleted: push(Qt.resolvedUrl("ui/MainMenu.qml"))
}
