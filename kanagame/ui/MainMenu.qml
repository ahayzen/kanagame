/*
 * Copyright (C) 2017
 *      Andrew Hayzen <ahayzen@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.4

import "../components"
import "../components/actions"

import "../logic/enum.js" as Enum

KanaPage {
    id: page

    KanaHeader {
        id: header
        leadingActions: [
            KanaHeadAction {
                hintColor: kanaTheme.palette.negative
                text: "X"
                textColor: kanaTheme.palette.negativeText

                onTriggered: Qt.quit()
            }
        ]
        title: "KanaGame"
        trailingActions: [
            KanaHeadAction {
                hintColor: kanaTheme.palette.neutral
                text: "?"
                textColor: kanaTheme.palette.neutralText

                onTriggered: {
                    kanaStack.push(
                        Qt.resolvedUrl("Help.qml"), {}
                    );
                }
            }
        ]
    }

    KanaMenu {
        anchors {
            top: header.bottom  // FIXME: make KanaPage.header
        }
        actions: [
            KanaAction {
                hintColor: kanaTheme.palette.neutral
                hintText: settings.highScore.kanaToRomaji
                text: "あ → a"
                textColor: kanaTheme.palette.neutralText

                onTriggered: {
                    kanaStack.push(
                        Qt.resolvedUrl("game/GameSelection.qml"),
                        {
                            "mode": Enum.KanaToRomaji,
                        }
                    );
                }
            },
            KanaAction {
                hintColor: kanaTheme.palette.positive
                hintText: settings.highScore.romajiToKana
                text: "a → あ"
                textColor: kanaTheme.palette.positiveText

                onTriggered: {
                    kanaStack.push(
                        Qt.resolvedUrl("game/GameSelection.qml"),
                        {
                            "mode": Enum.RomajiToKana,
                        }
                    );
                }
            }
        ]
    }

//    KanaSpinBox {
//        anchors {
//            bottom: parent.bottom
//            left: parent.left
//            margins: settings.display.gu(1)
//        }
//        minimum: 8
//        maximum: 18
//        step: 1
//        value: settings.display.scalar

//        onNewValue: settings.display.scalar = newValue
//    }

    Row {
        anchors {
            bottom: parent.bottom
            left: parent.left
            margins: 10 * 1
        }

        Repeater {
            delegate: Rectangle {
                anchors {
                    verticalCenter: parent.verticalCenter
                }
                border {
                    color: settings.display.scalar === modelData ? kanaTheme.palette.selected : "transparent"
                    width: modelData * 0.25
                }
                color: "transparent"
                height: modelData * 3
                width: height
                radius: height / 2

                Text {
                    anchors {
                        centerIn: parent
                    }
                    color: kanaTheme.palette.backgroundText
                    font {
                        pixelSize: modelData * 2
                    }
                    text: "あ"
                    verticalAlignment: Text.AlignVCenter
                }

                MouseArea {
                    anchors {
                        fill: parent
                    }
                    onClicked: settings.display.scalar = modelData
                }
            }
            model: {
                if ((page.width > 500 && page.height > 700) || settings.display.scalar === 16) {
                    [8, 10, 16]
                } else {
                    [8, 10]
                }
            }
        }
    }

    Row {
        anchors {
            bottom: parent.bottom
            margins: settings.display.gu(1)
            right: parent.right
        }
        spacing: settings.display.gu(1)

        Repeater {
            delegate: Rectangle {
                border {
                    color: kanaTheme.name === model.name ? kanaTheme.palette.selected : "transparent"
                    width: settings.display.gu(0.25)
                }
                color: model.color
                height: settings.display.gu(5)
                width: height
                radius: height / 2

                MouseArea {
                    anchors {
                        fill: parent
                    }
                    onClicked: settings.theme.name = model.name
                }
            }
            model: ListModel {
                ListElement {
                    color: "#000"
                    name: "Dark"
                }
                ListElement {
                    color: "#FFF"
                    name: "Light"
                }
            }
        }
    }
}
