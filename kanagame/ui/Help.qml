/*
 * Copyright (C) 2017
 *      Andrew Hayzen <ahayzen@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.4

import "../components"
import "../components/actions"

import "../logic/enum.js" as Enum
import "../logic/kana.js" as Kana
import "../logic/kanamap.js" as KanaMap

KanaPage {
    KanaHeader {
        id: header
        leadingActions: [
            KanaHeadAction {
                hintColor: kanaTheme.palette.negative
                text: "<"
                textColor: kanaTheme.palette.negativeText

                onTriggered: kanaStack.pop()
            }
        ]
    }

    KanaMenu {
        anchors.top: header.bottom  // FIXME: make KanaPage.header
        actions: [
            KanaAction {
                subtext: "hiragana"
                text: "あ"
                onTriggered: kanaStack.push(Qt.resolvedUrl("HelpChart.qml"),
                                            {"charset": KanaMap.hira, "language": Enum.Hiragana})
            },
            KanaAction {
                subtext: "katakana"
                text: "ア"
                onTriggered: kanaStack.push(Qt.resolvedUrl("HelpChart.qml"),
                                            {"charset": KanaMap.kata, "language": Enum.Katakana})
            }
        ]
    }
}
