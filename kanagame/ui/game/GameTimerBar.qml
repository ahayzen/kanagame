/*
 * Copyright (C) 2017
 *      Andrew Hayzen <ahayzen@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.4

Item {
    anchors {
        margins: settings.display.gu(1)
    }
    height: settings.display.gu(1)

    readonly property int score: roundTimer.score

    function startCountdown() {
        roundTimer.score = roundTimer.startScore;
        roundTimer.start();
    }

    function stop() {
        roundTimer.stop();
    }

    Text {
        id: roundTimerLabel
        anchors {
            left: parent.left
            verticalCenter: parent.verticalCenter
        }
        color: kanaTheme.palette.backgroundText
        text: roundTimer.score + (roundTimer.score < 100 ? " " : "") + (roundTimer.score < 10 ? " " : "")
    }

    Rectangle {
        id: roundTimerBar
        anchors {
            left: roundTimerLabel.right
            leftMargin: settings.display.gu(2)
            verticalCenter: parent.verticalCenter
        }
        color: kanaTheme.palette.neutral
        height: settings.display.gu(0.5)
        width: roundTimer.percentage * (parent.width - x)

        Behavior on width {
            NumberAnimation {
                duration: roundTimer.interval
            }
        }
    }

    Timer {
        id: roundTimer
        interval: 100
        repeat: true

        onTriggered: {
            score--;

            if (score === 0) {
                stop();
            }
        }

        readonly property double percentage: score / startScore
        readonly property int startScore: 100
        property int score: startScore
    }
}
