/*
 * Copyright (C) 2017
 *      Andrew Hayzen <ahayzen@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.4

import "../../components"
import "../../components/actions"

import "../../logic/kana.js" as Kana
import "../../logic/enum.js" as Enum

KanaPage {
    id: gamePage
    focus: true

    // TODO: abstract into GameSession
    property int mode
    property int score: 0

    // TODO: abstract into GamePool
    property var poolOrder      // [(roman, kana, poolID)...]
    property var pools          // Pools of tuples of roman and kana

    property int index: 0       // Index of which tuple to use

    signal userExit()
    signal finishScene(int mode, int score)

    onFinishScene: {
        kanaStack.push(Qt.resolvedUrl("GameFinish.qml"), {mode: mode, score: score})
    }

    onUserExit: kanaStack.pop()

    // NOTE: answeredWith uses indexes (0-3) human uses (1-4)
    Keys.onDigit1Pressed: gameGrid.humanAnsweredWith(0)
    Keys.onDigit2Pressed: gameGrid.humanAnsweredWith(1)
    Keys.onDigit3Pressed: gameGrid.humanAnsweredWith(2)
    Keys.onDigit4Pressed: gameGrid.humanAnsweredWith(3)

    Keys.onEscapePressed: gamePage.userExit()

    KanaHeader {
        id: header
        leadingActions: [
            KanaHeadAction {
                hintColor: kanaTheme.palette.negative
                text: "X"
                textColor: kanaTheme.palette.negativeText

                onTriggered: gamePage.userExit()
            }
        ]
        trailingActions: [
            KanaHeadAction {
                hintColor: kanaTheme.palette.neutral
                text: gamePage.score
                textColor: kanaTheme.palette.neutralText
            }
        ]
    }

    KanaGrid {
        id: gameGrid
        anchors {
            centerIn: parent
        }
        delegate: KanaButton {
            action: KanaAction {
                id: thisAction
                hintColor: {
                    if (correct) {
                        kanaTheme.palette.positive
                    } else if (selected) {
                        kanaTheme.palette.selected
                    } else {
                        kanaTheme.palette.widget
                    }
                }
                hintText: index + 1
                text: {
                    if (mode === Enum.KanaToRomaji) {
                        Kana.extractRomajiSingle(modelData);
                    } else if (mode === Enum.RomajiToKana) {
                        Kana.extractKanaSingle(modelData);
                    } else {
                        console.warn("Unknown game mode", mode);
                    }
                }
                textColor: {
                    if (correct) {
                        kanaTheme.palette.positiveText
                    } else if (selected) {
                        kanaTheme.palette.selectedText
                    } else {
                        kanaTheme.palette.widgetText
                    }
                }

                property bool correct: false
                property bool selected: false

                onTriggered: gameGrid.humanAnsweredWith(index);
            }

            Connections {
                target: gameGrid

                onAnsweredWith: {
                    if (i === index) {
                        thisAction.selected = true;
                    }
                }
                onCorrectAnswer: {
                    if (i === index) {
                        thisAction.correct = true
                    }
                }
            }
        }
        height: cellHeight * 2
        width: cellWidth * 2

        property int answer
        property bool canAnswer: true
        property int guess

        signal answeredWith(int i)
        signal correctAnswer(int i)
        signal correct()
        signal humanAnsweredWith(int i)
        signal incorrect()

        // User answered, store guess, stop timebar and show answer
        onAnsweredWith: {
            guess = i;

            timerBar.stop();
            delayAnswer.start();
        }
        // When the answer was correct increase the score
        onCorrect: parent.score += timerBar.score
        // Check if the stored guess was the same as the answer
        onCorrectAnswer: {
            if (guess === answer) {
                correct();
            } else {
                incorrect();
            }
        }
        // Human answerered - protect against double pushes (canAnswer)
        onHumanAnsweredWith: {
            if (canAnswer) {
                // once user has selected an answer it cannot be changed
                canAnswer = false;

                answeredWith(i);
            }
        }
        // When the answer was incorrect decrease the score
        onIncorrect: parent.score -= 100

        Timer {
            id: delayAnswer
            interval: 500

            onTriggered: {
                parent.correctAnswer(parent.answer);

                delayAnswerHint.start();
            }
        }

        Timer {
            id: delayAnswerHint
            interval: 500

            onTriggered: {
                answerHint.kana = Kana.extractKanaSingle(gameGrid.model[parent.answer]);
                answerHint.roman = Kana.extractRomajiSingle(gameGrid.model[parent.answer]);
                answerHint.visible = true;

                delayNextRound.start();
            }
        }

        Timer {
            id: delayNextRound
            interval: 1250

            onTriggered: {
                answerHint.visible = false;

                generateRound();
                parent.canAnswer = true;
            }
        }
    }

    KanaButton {
        id: answerHint
        anchors {
            centerIn: parent
        }
        action: KanaAction {
            hintColor: kanaTheme.palette.background
            text: answerHint.kana + "  " + answerHint.roman
            textColor: kanaTheme.palette.backgroundText
        }
        height: gameGrid.cellHeight
        width: gameGrid.cellWidth
        visible: false

        property string kana
        property string roman
    }

    GameTimerBar {
        id: timerBar
        anchors {
            left: gameGrid.left
            right: gameGrid.right
            top: gameGrid.bottom
            topMargin: settings.display.gu(2)
        }
    }

    function indexFromSet(key, pool)
    {
        for (var i=0; i < pool.length; i++) {
            if (key[0] === pool[i][0] && key[1] === pool[i][1]) {
                return i;
            }
        }

        return -1;
    }

    function generateRound()
    {
        if (index >= poolOrder.length) {
            finishScene(mode, score);
            return;
        }

        var answer = poolOrder[index];
        var options = Kana.round(pools[answer[2]], [answer[0], answer[1]]);

        console.debug("[DEBUG]", JSON.stringify(answer));

        // Ensure grid is cleared before updating
        // otherwise if new model is same as old selections are not updated
        gameGrid.model = [];
        gameGrid.model = options;

        if (mode === Enum.KanaToRomaji) {
            header.title = Kana.extractKanaSingle(answer);
        } else if (mode === Enum.RomajiToKana) {
            header.title = Kana.extractRomajiSingle(answer);
        } else {
            console.warn("Unknown game mode", mode);
        }

        gameGrid.answer = indexFromSet(answer, options);  // options.indexOf(answer);

        timerBar.startCountdown();

        index++;
    }

    Component.onCompleted: {
        forceActiveFocus();  // force focus to the page for keyboard shortcuts

        // Merge pools together and add ID as column
        var mergedPools = [];

        for (var i=0; i < pools.length; i++) {
            for (var j=0; j < pools[i].length; j++) {
                var tuple = pools[i][j].slice();
                tuple.push(i);
                mergedPools.push(tuple);

                console.debug("Pushing:", JSON.stringify(tuple));
            }

            console.debug("Pool:", i);
        }

        poolOrder = Kana.randomise(mergedPools);

        console.debug("Order:", JSON.stringify(poolOrder));

        generateRound();
    }
}

