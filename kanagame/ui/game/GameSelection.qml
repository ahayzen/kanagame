/*
 * Copyright (C) 2017
 *      Andrew Hayzen <ahayzen@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.4

import "../../components"
import "../../components/actions"

import "../../logic/kana.js" as Kana
import "../../logic/kanamap.js" as KanaMap
import "../../logic/enum.js" as Enum

KanaPage {
    property int mode

    property var selectedItems: settings.game.selection.split(",")

    signal selectAll(int pool, int amount)
    signal selectItem(var set)

    onSelectAll: {
        var i, j;
        var list = selectedItems;

        if (containsAll(pool, amount)) {
            // Remove All
            for (i=0; i < amount; i++) {
                j = itemIndex(pool, i);

                if (j !== -1) {
                    list.splice(j, 1);
                }
            }
        } else {
            // Add All
            for (i=0; i < amount; i++) {
                j = itemIndex(pool, i);

                if (j === -1) {
                    list.push(makeIndex(pool, i));
                }
            }
        }

        selectedItems = list;
    }
    onSelectItem: {
        var index = itemIndex(set[1], set[0]);
        var list = selectedItems;

        if (index === -1) {
            list.push(makeIndex(set[1], set[0]))  // add
        } else {
            list.splice(index, 1);  // remove
        }

        selectedItems = list;
    }
    onSelectedItemsChanged: {
        var selectedItemsString = selectedItems.join(",");

        if (settings.game.selection !== selectedItemsString) {
            settings.game.selection = selectedItemsString;
        }
    }

    function containsAll(pool, amount)
    {
        for (var i=0; i < amount; i++) {
            if (selectedItems.indexOf(makeIndex(pool, i)) === -1) {
                return false
            }
        }

        return true
    }

    function getSelected()
    {
        // FIXME: load sets from model
        var sets = [KanaMap.hira, KanaMap.kata];
        var output = [];

        // Scan for each set so we can group them easily
        for (var i=0; i < sets.length; i++) {
            var setOutput = []

            for (var j=0; j < selectedItems.length; j++) {
                if (selectedItems[j][0] === i.toString()) {
                    var k = parseInt(selectedItems[j].slice(2));
                    setOutput.push(sets[i][k]);
                }
            }

            output.push(Kana.concat(setOutput));
        }

        return output;
    }

    function itemIndex(pool, index)
    {
        return selectedItems.indexOf(makeIndex(pool, index));
    }

    function makeIndex(pool, index)
    {
        return pool.toString() + "-" + index.toString()
    }

    function sectionCount(index)
    {
        var count = 0;

        for (var i=0; i < selectedItems.length; i++) {
            if (selectedItems[i][0] === index.toString()) {
                count++;
            }
        }

        return count;
    }

    KanaHeader {
        id: header
        leadingActions: [
            KanaHeadAction {
                hintColor: kanaTheme.palette.negative
                text: "<"
                textColor: kanaTheme.palette.negativeText

                onTriggered: kanaStack.pop()
            }
        ]
        sections: [
            KanaHeadAction {
                text: "<b>ひらがな</b>　(hiragana) " + sectionCount(0) + "/" + KanaMap.hira.length
            },
            KanaHeadAction {
                text: "<b>カタカナ</b>　(katakana) " + sectionCount(1) + "/" + KanaMap.kata.length
            }
        ]
        title: {
            if (mode === Enum.KanaToRomaji) {
                "あ → a"
            } else if (mode === Enum.RomajiToKana) {
                "a → あ"
            } else {
                ""
            }
        }
        trailingActions: [
            KanaHeadAction {
                enabled: selectedItems.length > 0
                hintColor: kanaTheme.palette.positive
                text: ">"
                textColor: kanaTheme.palette.positiveText

                onTriggered: {
                    kanaStack.push(
                        Qt.resolvedUrl("Game.qml"),
                        {
                            "mode": mode,
                            "pools": getSelected(),
                        }
                    );
                }
            }
        ]
    }

    Flickable {
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            top: header.bottom
        }
        clip: true
        contentHeight: column.implicitHeight + column.anchors.topMargin + column.anchors.margins

        Column {
            id: column
            anchors {
                left: parent.left
                margins: settings.display.gu(2)
                right: parent.right
                top: parent.top
            }
            spacing: settings.display.gu(2)

            Repeater {
                delegate: Column {
                    spacing: settings.display.gu(0.5)
                    visible: header.sectionIndex === index
                    width: parent.width

                    readonly property int outerIndex: index

                    KanaSelectableRow {
                        height: settings.display.gu(5)
                        text: modelData["name"]
                        width: parent.width

                        readonly property bool selected: containsAll(index, modelData["set"].length)

                        onClicked: selectAll(index, modelData["set"].length)
                    }

                    Repeater {
                        id: flowRepeater
                        delegate: KanaSelectableRow {
                            height: settings.display.gu(4)
                            text: makeString(modelData)
                            x: settings.display.gu(2)
                            width: parent.width - x

                            readonly property bool selected: selectedItems.indexOf(makeIndex(outerIndex, index.toString())) !== -1

                            onClicked: selectItem([index, outerIndex])

                            function makeString(data)
                            {
                                var string = "";

                                for (var i=0; i < data.length; i++) {
                                    string += "<b>" + Kana.extractKanaSingle(data[i]) + "</b> (" + Kana.extractRomajiSingle(data[i]) + ") ";
                                }

                                return string;
                            }
                        }
                        model: modelData["set"]
                    }
                }

                model: [
                    {"name": "<b>ひらがな</b>　(hiragana)", "set": KanaMap.hira},
                    {"name": "<b>カタカナ</b>　(katakana)", "set": KanaMap.kata}
                ]
            }
        }
    }
}

