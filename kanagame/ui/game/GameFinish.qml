/*
 * Copyright (C) 2017
 *      Andrew Hayzen <ahayzen@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.4
import Qt.labs.settings 1.0

import "../../components"
import "../../components/actions"

import "../../logic/enum.js" as Enum

KanaPage {

    property int mode
    property int score

    KanaHeader {
        trailingActions: [
            KanaHeadAction {
                hintColor: kanaTheme.palette.positive
                text: ">"
                textColor: kanaTheme.palette.positiveText

                onTriggered: kanaStack.popToRoot()
            }
        ]
    }

    Text {
        anchors {
            centerIn: parent
        }
        color: kanaTheme.palette.backgroundText
        font {
            pixelSize: settings.display.gu(4)
        }
        text: score
    }

    Component.onCompleted: {
        if (mode === Enum.KanaToRomaji) {
            if (score > settings.highScore.kanaToRomaji) {
                settings.highScore.kanaToRomaji = score;
            }
        } else if (mode === Enum.RomajiToKana) {
            if (score > settings.highScore.romajiToKana) {
                settings.highScore.romajiToKana = score;
            }
        }
    }
}
