/*
 * Copyright (C) 2017
 *      Andrew Hayzen <ahayzen@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.4

import "../components"
import "../components/actions"

import "../logic/enum.js" as Enum

KanaPage {
    property var charset
    property string language

    KanaHeader {
        id: header
        leadingActions: [
            KanaHeadAction {
                hintColor: kanaTheme.palette.negative
                text: "<"
                textColor: kanaTheme.palette.negativeText

                onTriggered: kanaStack.pop()
            }
        ]
        title: language
    }

    Flickable {
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            top: header.bottom
        }
        clip: true
        contentHeight: column.implicitHeight + column.anchors.topMargin + column.anchors.margins

        Column {
            id: column
            anchors {
                left: parent.left
                margins: settings.display.gu(2)
                right: parent.right
                top: parent.top
                topMargin: settings.display.gu(4)
            }
            spacing: settings.display.gu(2)

            Repeater {
                delegate: Flow {
                    anchors {
                        horizontalCenter: parent.horizontalCenter
                    }
                    spacing: settings.display.gu(1)
                    // number of columns that fits in parent width (min=1) * cellWidth
                    width: Math.max(Math.min(Math.floor(column.width / cellWidth), count), 1) * (cellWidth + settings.display.gu(1))

                    readonly property int cellWidth: settings.display.gu(10)
                    readonly property int count: flowRepeater.count

                    Repeater {
                        id: flowRepeater
                        delegate: Item {
                            height: settings.display.gu(5)
                            width: settings.display.gu(10)

                            Row {
                                anchors {
                                    centerIn: parent
                                }
                                spacing: settings.display.gu(0.5)

                                Text {
                                    color: kanaTheme.palette.backgroundText
                                    font {
                                        pixelSize: settings.display.gu(3)
                                    }
                                    height: settings.display.gu(5)
                                    text: modelData[1]
                                    verticalAlignment: Text.AlignVCenter
                                }

                                Text {
                                    color: kanaTheme.palette.backgroundText
                                    font {
                                        pixelSize: settings.display.gu(1.5)
                                    }
                                    height: settings.display.gu(5)
                                    text: "(" + modelData[0] + ")"
                                    verticalAlignment: Text.AlignVCenter
                                }
                            }
                        }
                        model: modelData
                    }
                }
                model: charset
            }
        }
    }
}
